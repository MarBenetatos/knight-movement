package com.example.knightmovement.logic;



import com.example.knightmovement.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Algorithm {
    ArrayList<TreeNode> stack;
    ArrayList<String> move_sequences;

    String start_position = null;
    String end_position = null;
    int max_number_of_moves = -1;

    public Algorithm(String start, String end, int max_moves) {
        this.start_position = start;
        this.end_position = end;
        this.max_number_of_moves = max_moves;

        stack = new ArrayList<TreeNode>();
        move_sequences = new ArrayList<String>();
    }

    public ArrayList<String> start() {
        Utils.createLetterMapping();

        int start_position_letter = Utils.convertLetterToPosition(start_position);

        int start_position_number = Integer.parseInt(start_position.substring(1));

        int end_position_letter = Utils.convertLetterToPosition(end_position);
        int end_position_number = Integer.parseInt(end_position.substring(1));

        TreeNode root = new TreeNode(start_position_letter, start_position_number);
        createMovesetTree(root, 1, max_number_of_moves, end_position_letter, end_position_number);

        findMovePaths(root, 1, max_number_of_moves);

        return move_sequences;
    }

    public Set<TreeNode> findAvailableMoves(int position_letter, int position_number,
                                                   int end_position_letter, int end_position_number) {
        Set<TreeNode> possibleMoves = new HashSet<TreeNode>();


        if(position_letter-2 > 0) {

            if(position_number-1 > 0) {

                if((position_letter-2 == end_position_letter) && (position_number-1 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter-2, position_number-1, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter-2, position_number-1));
                }
            }
            if(position_number+1 <= 8) {

                if((position_letter-2 == end_position_letter) && (position_number+1 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter-2, position_number+1, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter-2, position_number+1));
                }
            }
        }
        if(position_letter+2 <= 8) {

            if(position_number-1 > 0) {

                if((position_letter+2 == end_position_letter) && (position_number-1 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter+2, position_number-1, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter+2, position_number-1));
                }
            }
            if(position_number+1 <= 8) {

                if((position_letter+2 == end_position_letter) && (position_number+1 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter+2, position_number+1, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter+2, position_number+1));
                }
            }
        }
        if(position_number-2 > 0) {

            if(position_letter-1 > 0) {

                if((position_letter-1 == end_position_letter) && (position_number-2 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter-1, position_number-2, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter-1, position_number-2));
                }
            }
            if(position_letter+1 <= 8) {

                if((position_letter+1 == end_position_letter) && (position_number-2 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter+1, position_number-2, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter+1, position_number-2));
                }
            }
        }
        if(position_number+2 <= 8) {

            if(position_letter-1 > 0) {

                if((position_letter-1 == end_position_letter) && (position_number+2 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter-1, position_number+2, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter-1, position_number+2));
                }
            }
            if(position_letter+1 <= 8) {

                if((position_letter+1 == end_position_letter) && (position_number+2 == end_position_number)) {

                    possibleMoves.add(new TreeNode(position_letter+1, position_number+2, true));
                }
                else {
                    possibleMoves.add(new TreeNode(position_letter+1, position_number+2));
                }
            }
        }

        return possibleMoves;
    }

    public void createMovesetTree(TreeNode root, int depth, int max_depth, int end_position_letter, int end_position_number) {
        if(depth > max_depth) {
            return;
        }


        root.addChildrenNodes(findAvailableMoves(root.getPositionLetter(), root.getPositionNumber(), end_position_letter, end_position_number));

        Iterator<TreeNode> iterator = root.getChildrenNodes().iterator();
        while(iterator.hasNext()) {
            TreeNode childNode = iterator.next();
            if(!childNode.isLeaf()) {
                createMovesetTree(childNode, depth+1, max_depth, end_position_letter, end_position_number);
            }
        }
    }

    public void findMovePaths(TreeNode root, int depth, int max_depth) {
        if(depth > max_depth) {
            return;
        }

        stack.add(root);

        Iterator<TreeNode> iterator = root.getChildrenNodes().iterator();
        while(iterator.hasNext()) {
            TreeNode childNode = iterator.next();
            if(!childNode.isLeaf()) {
                findMovePaths(childNode, depth+1, max_depth);
            }
            else {
                stack.add(childNode);
                saveMoveSequence();
                stack.remove(stack.size()-1);
            }
        }
        stack.remove(stack.size()-1);
    }

    public void saveMoveSequence() {
        String move_seq = "";

        for(int i=0;i<stack.size();i++) {
            if(i == stack.size()-1) {
                move_seq = move_seq.concat(stack.get(i).toChessString());
            }
            else {
                move_seq = move_seq.concat(stack.get(i).toChessString()+"->");
            }
        }
        move_sequences.add(move_seq);
    }
}
